//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StudentWebControl
{
    using System;
    using System.Collections.Generic;
    
    public partial class ThingsToGo
    {
        public int Id { get; set; }
        public int WilGo { get; set; }
        public int EventsId { get; set; }
        public bool YesOrNo { get; set; }
        public Nullable<int> ClassId { get; set; }
    
        public virtual Class Class { get; set; }
        public virtual Event Event { get; set; }
        public virtual Student Student { get; set; }
        public virtual User User { get; set; }
    }
}
