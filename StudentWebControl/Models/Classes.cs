﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Classes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }
}