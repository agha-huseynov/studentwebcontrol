﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class ThingsToGo
    {
        public int WillGo { get; set; }
        public int EventId { get; set; }
        public string  EventName { get; set; }
        public bool YesOrNo { get; set; }
        public int ClassId { get; set; }
        public string WillGoName { get; set; }
    }   
}