﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Homework
    {
        public string WorkContent { get; set; }
        public int TeacherId { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int ClassId { get; set; }
        public string Lesson { get; set; }
    }
}