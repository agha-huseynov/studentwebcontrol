﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Phone { get; set; }
        public string Telephone { get; set; }
        public string Adress { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? ClassId { get; set; }
        public byte[] Image { get; set; }
        public string ParentName { get; set; }
        public string ParentSurname { get; set; }
        public HttpPostedFileBase StudentCoverImage { get; set; }
    }
}