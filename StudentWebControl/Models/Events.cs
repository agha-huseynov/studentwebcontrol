﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Events
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime? EventDate { get; set; }
        public int? TeacherId { get; set; }
        public int? ClassId { get; set; }
        public string EventContent { get; set; }
    }
}