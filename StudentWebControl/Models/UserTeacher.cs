﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Models
{
    public class UserTeacher
    {
        public Models.User user { get; set; }
        public Models.Teacher teacher { get; set; }
    }
}