﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Telephone { get; set; }
        public string Brahch { get; set; }
        public int ClassId { get; set; }
        public byte[] Image { get; set; }
    }
}