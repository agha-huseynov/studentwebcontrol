﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Video
    {
        public string URL { get; set; }
        public string Name { get; set; }
        public byte[] Image { get; set; }
    }
}