﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Notfication
    {
        public DateTime? Date { get; set; }
        public string MessageContent { get; set; }
    }
}