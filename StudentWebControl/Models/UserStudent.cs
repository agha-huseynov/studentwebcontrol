﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Models
{
    public class UserStudent
    {
        public Models.User user { get; set; }
        public Models.Student student { get; set; }
    }
}