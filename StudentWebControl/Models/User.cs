﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public int? ClassId { get; set; }
        public int? UserType { get; set; }
    }
}