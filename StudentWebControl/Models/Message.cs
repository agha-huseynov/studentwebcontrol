﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentControl.Model
{
    public class Message
    {
        public string MessageContent { get; set; } 
        public string Sender { get; set; }
        public string Receiver { get; set; }
    }
}