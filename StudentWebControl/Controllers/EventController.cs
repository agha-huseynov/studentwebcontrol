﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentWebControl.Controllers
{
    public class EventController : Controller
    {
        StudentControlDBEntities db = new StudentControlDBEntities();
        // GET: Event
        public ActionResult Index()
        {
            return View(db.Events.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}