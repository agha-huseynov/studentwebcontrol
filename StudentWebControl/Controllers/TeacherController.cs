﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using StudentControl.Models;

namespace StudentWebControl.Controllers
{
    public class TeacherController : Controller
    {
        StudentControlDBEntities db = new StudentControlDBEntities();
        // GET: Teacher
        public async Task<ActionResult> Index()
        {
            return View(await db.Teachers.ToListAsync());
        }

        public async Task<ActionResult> Create()
        {
            ViewData["Classes"] = new SelectList(await db.Classes.OrderBy(p => p.Name).ToArrayAsync(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserTeacher userTeacher)
        {
            var usr = new User
            {
                Name = userTeacher.user.Name,
                Password = userTeacher.user.Password,
                EMail = userTeacher.user.EMail,
                Surname = userTeacher.user.Surname,
                Username = userTeacher.user.Username,
                UserType = 1,
            };
            db.Users.Add(usr);

            var tch = new Teacher
            {
               Branch = userTeacher.teacher.Brahch,
               ClassId = userTeacher.teacher.ClassId,
               Id = usr.Id,
               Telephone = userTeacher.teacher.Telephone,
            };
            db.Teachers.Add(tch);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}