﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using StudentControl.Model;
using StudentControl.Models;

namespace StudentWebControl.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student\
        StudentControlDBEntities db = new StudentControlDBEntities();

        public async Task<ActionResult> Index()
        {
            List<StudentControl.Models.Student> student = await (from t in db.Students
                                                                 select new StudentControl.Models.Student
                                                                 {
                                                                     Id = t.Id,
                                                                     Adress = t.Adress,
                                                                     DateOfBirth = t.BornDate,
                                                                     ClassId = t.ClassId,
                                                                     Image = t.Image,
                                                                     ParentName = t.ParentName,
                                                                     ParentSurname = t.ParentSurname,
                                                                     Phone = t.Phone,
                                                                     Telephone = t.Phone
                                                                 }).ToListAsync();
            return View(student);
        }

        public async Task<ActionResult> Creaate()
        {
            ViewData["Classes"] = new SelectList(await db.Classes.OrderBy(p => p.Name).ToArrayAsync(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Creaate(UserStudent userStudent)
        {
            var usr = new User
            {
                Name = userStudent.user.Name,
                Password = userStudent.user.Password,
                EMail = userStudent.user.EMail,
                Surname = userStudent.user.Surname,
                Username = userStudent.user.Username,
                UserType = 1,
            };
            db.Users.Add(usr);
            await db.SaveChangesAsync();

            var std = new Student
            {
                Phone = userStudent.student.Phone,
                Adress = userStudent.student.Adress,
                BornDate = userStudent.student.DateOfBirth,
                ClassId = userStudent.student.ClassId,
                Id = usr.Id,
                ParentName = userStudent.student.ParentName,
                ParentSurname = userStudent.student.ParentSurname,
                Telephone = userStudent.student.Telephone,
            };
            db.Students.Add(std);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}