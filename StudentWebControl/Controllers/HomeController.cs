﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using StudentControl.Model;
using Newtonsoft.Json;

namespace StudentWebControl.Controllers
{
    public class HomeController : Controller
    {
        StudentControlDBEntities db = new StudentControlDBEntities();
        // GET: Home
        public async Task<ActionResult> Index()
        {
            return View(await db.Classes.OrderByDescending(q => q.Id).ToListAsync());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Class clss)
        {
            db.Classes.Add(clss);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


    }
}